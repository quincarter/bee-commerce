// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBXMPvmkqgG3aUs40X0nZA92Zurw32YIcY',
    authDomain: 'bee-commerce.firebaseapp.com',
    databaseURL: 'https://bee-commerce.firebaseio.com',
    projectId: 'bee-commerce',
    storageBucket: 'bee-commerce.appspot.com',
    messagingSenderId: '98974500740',
    appId: '1:98974500740:web:691b1b3955231391283fd1',
    measurementId: 'G-C1EFBH763V',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
