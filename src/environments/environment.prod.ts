export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyBXMPvmkqgG3aUs40X0nZA92Zurw32YIcY',
    authDomain: 'bee-commerce.firebaseapp.com',
    databaseURL: 'https://bee-commerce.firebaseio.com',
    projectId: 'bee-commerce',
    storageBucket: 'bee-commerce.appspot.com',
    messagingSenderId: '98974500740',
    appId: '1:98974500740:web:691b1b3955231391283fd1',
    measurementId: 'G-C1EFBH763V',
  },
};
