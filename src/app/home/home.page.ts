import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { ShopService } from '../services/state/shop/shop.service';
import { Observable } from 'rxjs';
import { ShopQuery } from '../services/state/shop/shop.query';
import { Shop } from '../services/state/shop/shop.model';
import { IonRouterOutlet, ModalController } from '@ionic/angular';
import { CartPage } from '../pages/cart/cart.page';
import { CartQuery } from '../services/state/cart.query';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  public inventory$: Observable<Shop[]>;
  public cartCount = 0;
  constructor(
    private data: DataService,
    private shopService: ShopService,
    private shopQuery: ShopQuery,
    private cartQuery: CartQuery,
    private modalController: ModalController,
    private routerOutlet: IonRouterOutlet
  ) {}

  ngOnInit() {
    this.shopService.syncCollection().subscribe();
    this.inventory$ = this.shopQuery.selectAll();
    this.cartQuery.selectAll().subscribe((data) => {
      this.cartCount = 0;
      data.forEach((item) => {
        this.cartCount += item.quantity;
      });

      if (data.length === 0) {
        this.cartCount = 0;
      }
    });
  }

  refresh(ev) {
    setTimeout(() => {
      ev.detail.complete();
    }, 3000);
  }

  async openCart() {
    const modal = await this.modalController.create({
      component: CartPage,
      cssClass: 'my-custom-class',
      mode: 'ios',
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
    });
    return await modal.present();
  }
}
