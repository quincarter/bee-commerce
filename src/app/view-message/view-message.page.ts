import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../services/data.service';
import { Shop } from '../services/state/shop/shop.model';
import { ShopService } from '../services/state/shop/shop.service';
import { ShopQuery } from '../services/state/shop/shop.query';
import { CartService } from '../services/state/cart.service';
import { CartQuery } from '../services/state/cart.query';
import { IonRouterOutlet, ModalController } from '@ionic/angular';
import { CartPage } from '../pages/cart/cart.page';
import { Cart } from '../services/state/cart.model';
import { Title } from '@angular/platform-browser';

export interface HoneyOptions {
  title: string;
  price: number;
  image?: string;
}

export const HONEY_OPTIONS: HoneyOptions[] = [
  {
    title: '1oz Bear',
    price: 1.0,
  },
  {
    title: '4oz Bear',
    price: 4.0,
  },
  {
    title: '8oz Bear',
    price: 8.0,
  },
  {
    title: '12oz Bear',
    price: 12.0,
  },
  {
    title: '16oz Bottle',
    price: 16.0,
  },
  {
    title: '24oz Bottle',
    price: 24.0,
  },
  {
    title: '32oz Bottle',
    price: 32.0,
  },
];

@Component({
  selector: 'app-view-message',
  templateUrl: './view-message.page.html',
  styleUrls: ['./view-message.page.scss'],
})
export class ViewMessagePage implements OnInit, OnDestroy {
  public shopItem: Shop;
  public honeyOptions: HoneyOptions[] = HONEY_OPTIONS;
  public price: number;
  public quantity = 1;

  selectBoxValue: any;
  @ViewChild('itemSelector') itemSelectorElement: ElementRef;
  cartCount = 0;

  constructor(
    private data: DataService,
    private activatedRoute: ActivatedRoute,
    private shopService: ShopService,
    private shopQuery: ShopQuery,
    private cartService: CartService,
    private cartQuery: CartQuery,
    private modalController: ModalController,
    private routerOutlet: IonRouterOutlet,
    private titleService: Title
  ) {}

  ngOnInit() {
    const shopId = this.activatedRoute.snapshot.paramMap.get('id');
    this.shopService.syncDoc({ id: shopId }).subscribe();
    this.shopItem = this.shopQuery.getEntity(shopId);
    this.titleService.setTitle(`Shop - ${this.shopItem.title}`);

    this.cartQuery.selectAll().subscribe((data) => {
      this.cartCount = 0;
      data?.forEach((item) => {
        this.cartCount += item.quantity;
      });

      if (data.length === 0) {
        this.cartCount = 0;
      }
    });
    if (typeof this.shopItem?.price === 'number') {
      this.setPrice(this.shopItem.price);
    }
  }

  ngOnDestroy() {
    this.titleService.setTitle('Bee Commerce');
  }

  getBackButtonText() {
    const win = window as any;
    const mode = win && win.Ionic && win.Ionic.mode;
    return mode === 'ios' ? 'Shop' : '';
  }

  setPrice(price: number | any) {
    if (typeof price !== 'number') {
      this.price = price.detail?.value?.price;
    } else {
      this.price = price;
    }
  }

  async addToCart(shopItem: Shop) {
    const price = shopItem.multipleOptions ? this.selectBoxValue.title : shopItem.price;
    const cartItem: Cart = {
      id: shopItem.title + '-' + price,
      title: shopItem.title,
      description: shopItem.description,
      selectedOption: this.selectBoxValue?.title,
      pricePerItem: this.price,
      quantity: this.quantity,
      image: shopItem.image,
      totalPrice: this.price * this.quantity,
    };

    this.selectBoxValue = null;
    this.price = typeof shopItem?.price === 'number' ? shopItem.price : null;

    const alreadyExists = this.cartQuery.getAll().find((item) => {
      if (item.id === cartItem.id) {
        const newTotal = cartItem.pricePerItem * (this.quantity + item.quantity);
        this.cartService.update(cartItem.id, {
          ...cartItem,
          quantity: this.quantity + item.quantity,
          totalPrice: newTotal,
        });
        this.showModal();
        return true;
      } else {
        return false;
      }
    });

    if (!alreadyExists) {
      console.log(cartItem);
      this.cartService.add(cartItem);
      this.showModal();
    }
  }

  async showModal(): Promise<void> {
    const modal = await this.modalController.create({
      component: CartPage,
      cssClass: 'my-custom-class',
      mode: 'ios',
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
    });
    return await modal.present();
  }
}
