import { Component, Input, OnInit } from '@angular/core';
import { Shop } from '../services/state/shop/shop.model';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss'],
})
export class MessageComponent implements OnInit {
  @Input() shopItem: Shop;

  constructor(private navController: NavController) {}

  ngOnInit() {}

  navigateForward(id: number | string) {
    this.navController.navigateForward(`/shop/${id.toString()}`);
  }
}
