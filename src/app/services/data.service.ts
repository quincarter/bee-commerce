import { Injectable } from '@angular/core';

export interface Message {
  multipleOptions?: boolean;
  title: string;
  subtitle: string;
  price?: number | string;
  id: number;
  description?: string;
  read: boolean;
  image?: string;
}

@Injectable({
  providedIn: 'root',
})
export class DataService {
  public messages: Message[] = [
    {
      title: 'Fresh Honey',
      subtitle: 'Multiple Sizes Available',
      price: null,
      id: 0,
      read: false,
      image: 'assets/img/honey-thumb.png',
      multipleOptions: true,
      description: `
        Fresh honey available in sizes ranging from 1oz to 32oz
      `,
    },
    {
      title: 'Whipped Honey',
      subtitle: 'Long time no chat',
      price: 9.99,
      id: 1,
      read: false,
      image: 'assets/img/creamed-honey.png',
      description:
        'Whipped honey is also called creamed honey. But no cream is added, this is just heavily whipped and very creamy when it is finished.',
    },
  ];

  constructor() {}

  public getMessages(): Message[] {
    return this.messages;
  }

  public getMessageById(id: number): Message {
    return this.messages[id];
  }
}
