import { ID } from '@datorama/akita';

export interface Cart {
  id: string;
  title: string;
  description: string;
  pricePerItem: number;
  quantity: number;
  image: string;
  selectedOption?: any;
  totalPrice: number;
}

export function createCart(params: Partial<Cart>) {
  return {} as Cart;
}
