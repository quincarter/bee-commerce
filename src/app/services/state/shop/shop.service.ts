import { Injectable } from '@angular/core';
import { ShopStore, ShopState } from './shop.store';
import { CollectionConfig, CollectionService } from 'akita-ng-fire';

@Injectable({ providedIn: 'root' })
@CollectionConfig({ path: 'inventory' })
export class ShopService extends CollectionService<ShopState> {
  constructor(store: ShopStore) {
    super(store);
  }
}
