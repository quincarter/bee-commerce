import { Injectable } from '@angular/core';
import { Shop } from './shop.model';
import { EntityState, ActiveState, EntityStore, StoreConfig } from '@datorama/akita';

export interface ShopState extends EntityState<Shop, string>, ActiveState<string> {}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'shop' })
export class ShopStore extends EntityStore<ShopState> {
  constructor() {
    super();
  }
}
