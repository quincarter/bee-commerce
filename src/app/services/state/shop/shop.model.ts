export interface Shop {
  multipleOptions?: boolean;
  title: string;
  subtitle: string;
  price?: number | string;
  id: number | string;
  description?: string;
  image?: string;
}

export function createShop(params: Partial<Shop>) {
  return {} as Shop;
}
