import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CartPageRoutingModule } from './cart-routing.module';

import { CartPage } from './cart.page';
import { HttpClientModule } from '@angular/common/http';
import { CartService } from '../../services/state/cart.service';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, HttpClientModule, CartPageRoutingModule],
  providers: [CartService],
  declarations: [CartPage],
})
export class CartPageModule {}
