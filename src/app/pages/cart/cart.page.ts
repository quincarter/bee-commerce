import { Component, OnInit } from '@angular/core';
import { CartQuery } from '../../services/state/cart.query';
import { CartService } from '../../services/state/cart.service';
import { Cart } from '../../services/state/cart.model';
import { AlertController, ModalController } from '@ionic/angular';
import { getEntityType } from '@datorama/akita';
import { CartState } from '../../services/state/cart.store';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})
export class CartPage implements OnInit {
  public cart$: Cart[];
  private totalInit = 0.0;
  public total: string;
  private cartCount = 0;
  constructor(
    private cartQuery: CartQuery,
    private cartService: CartService,
    private modalController: ModalController,
    private alertController: AlertController
  ) {}

  ngOnInit() {
    this.cartQuery.selectAll().subscribe((data) => {
      this.cart$ = data;
      this.setTotal(data);
      if (data.length > 0) {
        this.loadStripe();
      }

      this.cartCount = 0;
      data?.forEach((item) => {
        this.cartCount += item.quantity;
      });

      if (data.length === 0) {
        this.cartCount = 0;
      }
    });
  }

  closeModal() {
    this.modalController.dismiss();
  }

  async removeItem(id: string) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Remove Item From Cart?',
      message: '<strong>This will remove the item from your cart</strong>',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
        },
        {
          text: 'Remove',
          cssClass: 'danger',
          handler: () => {
            this.cartService.remove(id);
            this.setTotal(this.cart$);
          },
        },
      ],
    });

    await alert.present();
  }

  private setTotal(data: getEntityType<CartState>[] | Cart[]) {
    this.totalInit = 0;
    data.forEach((item) => {
      this.totalInit += item.totalPrice;
    });

    this.total = this.totalInit.toFixed(2);
  }

  loadStripe() {
    if (!window.document.getElementById('stripe-script')) {
      const s = window.document.createElement('script');
      s.id = 'stripe-script';
      s.type = 'text/javascript';
      s.src = 'https://checkout.stripe.com/checkout.js';
      window.document.body.appendChild(s);
    }
  }

  pay(amount) {
    const handler = (window as any).StripeCheckout.configure({
      key: 'pk_test_cfRm81l2lVC6ePPOVkFLyaJy',
      locale: 'auto',
      token(token: any) {
        // You can access the token ID with `token.id`.
        // Get the token ID to your server-side code for use.
        console.log(token);
        alert('Token Created!!');
      },
    });

    handler.open({
      name: 'PK Pure Honey',
      description: `${this.cartCount} item${this.cartCount > 1 ? 's' : ''}`,
      amount: amount * 100,
    });
  }
}
